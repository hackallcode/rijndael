# AES (Rijndael)

This tool provides encryption/decrytion according to AES (Rijndael) standart with 128/192/256 bit long keys.
[Link to the official document for details](http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf) 

## How to use

Example of encryption:
```shell script
python main.py -k my_key -i test.txt -o test.encrypted
```

Example of decryption:
```shell script
python main.py -k my_key -d -i test.encrypted -o test.decrypted
```

For more help information you can use `--help` flag.

# How to run tests

Just run PyTest:
```shell script
pytest
```

To select special test you can use `-k` flag:
```shell script
pytest -k aes_192
```

More you can see in [official documentation](https://docs.pytest.org/en/stable/)
