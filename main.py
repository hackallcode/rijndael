import argparse
import os

import aes_cipher
from debugger import DEBUG_LTX, DEBUG_HEX, DEBUG_STD, DEBUG_BIN, DEBUG_DEC


def main():
    parser = argparse.ArgumentParser(
        description='AES Cipher',
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        '-k', '--key', type=str, required=True,
        help='Key for encryption/decryption.\nMode of encryption is selected by key length:\n16 symbols for AES-128\n24 symbols for AES-192\n32 symbols for AES-256',
    )
    parser.add_argument(
        '-d', '--decrypt', action='store_true',
        help='Use this flag to decrypt input file, by default will be encryption.',
    )
    parser.add_argument(
        '-i', '--input', type=str, default='data/text.txt',
        help='Name to file of input. "data/text.txt" by default.',
    )
    parser.add_argument(
        '-o', '--output', type=str,
        help='Path to file of output. "{INPUT}.encrypted" or "{INPUT}.decrypted" by default.',
    )
    parser.add_argument(
        '-m', '--debug_mode', type=int, choices=[1, 2],
        help='Format of debug out:\n1 - Markdown\n2 - LaTeX\nDisabled by default.',
    )
    parser.add_argument(
        '-n', '--number_format', type=int, default=2, choices=[1, 2, 3],
        help='Format of numbers to debug:\n1 - binary\n2 - decimal\n3 - hexadecimal\nDecimal by default.',
    )
    args = parser.parse_args()

    if len(args.key) <= 16:
        mode = aes_cipher.AES_128
    elif len(args.key) <= 24:
        mode = aes_cipher.AES_192
    elif len(args.key) <= 32:
        mode = aes_cipher.AES_256
    else:
        raise Exception('Key length is invalid!')

    debug_mode = 0
    if args.debug_mode == 1:
        debug_mode = DEBUG_STD
    elif args.debug_mode == 2:
        debug_mode = DEBUG_LTX
    if args.debug_mode:
        if args.number_format == 1:
            debug_mode |= DEBUG_BIN
        elif args.number_format == 2:
            debug_mode |= DEBUG_DEC
        elif args.number_format == 3:
            debug_mode |= DEBUG_HEX

    aes = aes_cipher.AesCipher(mode=mode, debug_mode=debug_mode)

    input_path = os.path.abspath(args.input)
    with open(input_path, 'rb') as f:
        data = f.read()

    if not args.decrypt:
        suffix = '.encrypted'
        result = aes.encrypt(data, args.key.encode('utf-8'))
    else:
        suffix = '.decrypted'
        result = aes.decrypt(data, args.key.encode('utf-8'))

    output_path = args.output or os.path.splitext(input_path)[0] + suffix
    with open(output_path, 'wb') as f:
        f.write(result)


if __name__ == '__main__':
    main()
