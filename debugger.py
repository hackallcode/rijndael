DEBUG_STD = 1
DEBUG_LTX = 2

DEBUG_DEC = 4
DEBUG_BIN = 8
DEBUG_HEX = 16


class Debugger:
    def __init__(self, mode=0, title=None):
        self.change_mode(mode)
        self.start_document(title)

    def __del__(self):
        self.end_document()

    def pause(self):
        if self.__mode:
            self.__prev_mode = self.__mode
            self.__mode = 0

    def play(self):
        if self.__prev_mode and not self.__mode:
            self.__mode = self.__prev_mode

    def change_mode(self, mode):
        self.__mode = mode
        self.__prev_mode = None

    def debug(self, *args, **kwargs):
        if not self.__mode:
            return

        print(*args, **kwargs)

    def start_document(self, title=None):
        if self.__mode & DEBUG_LTX:
            self.debug('\\documentclass[fleqn]{report}')
            self.debug('\\usepackage{amsmath}')
            self.debug('\\setlength\\parindent{0pt}')
            self.debug('\\setlength{\\mathindent}{0pt}')
            if title is not None:
                self.debug(f'\\title{{{title}}}')
            self.debug('\\begin{document}')
            if title is not None:
                self.debug('\\maketitle')

    def end_document(self):
        if self.__mode & DEBUG_LTX:
            self.debug('\\end{document}')

    def chapter(self, name):
        if not self.__mode:
            return

        if self.__mode & DEBUG_LTX:
            self.debug(f'\\chapter*{{{name}}}')
            return

        self.debug(f'# {name}')

    def section(self, name):
        if not self.__mode:
            return

        if self.__mode & DEBUG_LTX:
            self.debug(f'\\section*{{{name}}}')
            return

        self.debug(f'## {name}')

    def subsection(self, name):
        if not self.__mode:
            return

        if self.__mode & DEBUG_LTX:
            self.debug(f'\\subsection*{{{name}}}')
            return

        self.debug(f'### {name}')

    def subsubsection(self, name):
        if not self.__mode:
            return

        if self.__mode & DEBUG_LTX:
            self.debug(f'\\subsubsection*{{{name}}}')
            return

        self.debug(f'#### {name}')

    def paragraph(self, name):
        if not self.__mode:
            return

        if self.__mode & DEBUG_LTX:
            self.debug(f'\\paragraph*{{{name}}}')
            return

        self.debug(f'##### {name}')

    def subparagraph(self, name):
        if not self.__mode:
            return

        if self.__mode & DEBUG_LTX:
            self.debug(f'\\subparagraph*{{{name}}}')
            return

        self.debug(f'###### {name}')

    def i(self, integer, clear_mode=False):
        if clear_mode:
            return f"{integer}"
        if self.__mode & DEBUG_BIN:
            return f"{integer:08b}b"
        if self.__mode & DEBUG_HEX:
            return f"{integer:02X}h"
        return f"{integer:3}"

    def inverted(self, value, res):
        if not self.__mode:
            return

        if self.__mode & DEBUG_LTX:
            self.debug(f'\\begin{{equation*}}\n{self.i(value)}^{{-1}} = {self.i(res)}\n\\end{{equation*}}\n')
            return

        self.debug(f'{self.i(value)}^-1 = {self.i(res)}\n')

    def table(self, table, first=0):
        if not self.__mode:
            return

        size = len(table[0])

        if self.__mode & DEBUG_LTX:
            s = '\\begin{equation*}\n\\begin{bmatrix}\n'
            for i in range(first, first + size):
                for j in range(size):
                    s += self.i(table[i][j])
                    if j < size - 1:
                        s += ' & '
                    elif i < first + size - 1:
                        s += ' \\\\'
                s += '\n'
            s += '\\end{bmatrix}\n\\end{equation*}\n'
            self.debug(s)
            return

        s = ''
        for i in range(first, first + size):
            for j in range(size):
                s += self.i(table[i][j]) + ' '
            s += '\n'
        self.debug(s)

    def affine(self, r, a=None, b=None, c=None, clear_mode=False):
        if not self.__mode:
            return

        size = max(len(r), len(a or []), len(b or []), len(c or []))
        r = r[:]
        while len(r) < size:
            r.append(0)
        if a is not None:
            a = a[:]
            while len(a) < size:
                a.append([])
            for aa in a:
                while len(aa) < size:
                    aa.append(0)
        if b is not None:
            b = b[:]
            while len(b) < size:
                b.append(0)
        if c is not None:
            c = c[:]
            while len(c) < size:
                c.append(0)

        if self.__mode & DEBUG_LTX:
            s = '\\begin{equation*}\n\\begin{bmatrix}\n'
            for rr in r:
                s += f'{self.i(rr, clear_mode)} \\\\\n'
            s += '\\end{bmatrix}\n='
            if a is not None:
                s += '\\begin{bmatrix}\n'
                for i in range(size):
                    for j in range(size):
                        s += self.i(a[i][j], clear_mode)
                        if j < size - 1:
                            s += ' & '
                        elif i < size - 1:
                            s += ' \\\\'
                    s += '\n'
                s += '\\end{bmatrix}\n'
            if a is not None and b is not None:
                s += '\cdot'
            if b is not None:
                s += '\\begin{bmatrix}\n'
                for bb in b:
                    s += f'{self.i(bb, clear_mode)} \\\\\n'
                s += '\\end{bmatrix}\n'
            if (a is not None or b is not None) and c is not None:
                s += '+'
            if c is not None:
                s += '\\begin{bmatrix}\n'
                for cc in c:
                    s += f'{self.i(cc, clear_mode)} \\\\\n'
                s += '\\end{bmatrix}\n'
            s += '\\end{equation*}\n'
            self.debug(s)
            return

        s = ''
        for i in range(size):
            s += f'[{self.i(r[i], clear_mode)}]'
            if i == int(size / 2):
                s += ' = ['
            else:
                s += '   ['
            for j in range(size):
                s += f'{self.i(a[i][j], clear_mode)}'
                if j < size - 1:
                    s += ' '
            s += f'] [{self.i(b[i], clear_mode)}'
            if c is not None:
                if i == int(size / 2):
                    s += '] + ['
                else:
                    s += ']   ['
                s += f'{self.i(c[i], clear_mode)}'
            s += ']\n'
        self.debug(s)
