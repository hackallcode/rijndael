class BinPol:
    def __init__(self, x, irreducible_polynomial=None, grade=None):
        self.dec = x
        self.hex = hex(self.dec)[2:]
        self.bin = reversed(list(bin(self.dec)[2:]))
        self.bin = [int(bit) for bit in self.bin]

        if grade is not None:
            self.grade = grade
        else:
            self.grade = len(self.bin) - 1

        self.irreducible_polynomial = irreducible_polynomial

    def __str__(self):
        h = self.hex
        if self.dec < 16:
            h = '0' + h
        return h

    def __repr__(self):
        return str(self)

    def __len__(self):
        return self.grade

    def __remove_most_significant_zeros(self):
        last = 0
        for i, a in enumerate(self.bin):
            if a:
                last = i
        del (self.bin[last + 1:])

    def __update_from_bin(self):
        self.__remove_most_significant_zeros()

        self.dec = 0
        for i, a in enumerate(self.bin):
            if a:
                self.dec += 2 ** i

        self.hex = hex(self.dec)[2:]

        self.grade = len(self.bin) - 1

    def __setitem__(self, key, value):
        if value in [0, 1]:
            while len(self.bin) <= key:
                self.bin.append(0)

            self.bin[key] = value

        self.__update_from_bin()

    def __getitem__(self, key):
        if key < len(self.bin):
            return self.bin[key]
        else:
            return 0

    def __add__(self, x):
        r = BinPol(self.dec, self.irreducible_polynomial)
        for i, a in enumerate(x.bin):
            r[i] = r[i] ^ a
        return r

    def __mul__(self, x):
        r = BinPol(0, self.irreducible_polynomial)
        a = BinPol(self.dec)
        for b in x.bin:
            if b:
                r += a
            a = BinPol(self.dec << 1)
            if self.irreducible_polynomial and a.grade >= self.irreducible_polynomial.grade:
                a += self.irreducible_polynomial
        return r

    def __pow__(self, x):
        r = BinPol(1, self.irreducible_polynomial)
        for _ in range(x):
            r = r * BinPol(self.dec)
        return r


class IrrBinPol(BinPol):
    def __init__(self, x, primitive, grade=None):
        super().__init__(x, grade=grade)

        self.primitive = BinPol(primitive, self)
        self.anti_log = []
        self.log = [BinPol(0, x) for _ in range(2 ** self.grade)]
        for i in range(2 ** self.grade):
            value = self.primitive ** i
            self.anti_log.append(value)
            self.log[value.dec] = BinPol(i, x)

    def inv(self, pol):
        if pol.dec == 0:
            return BinPol(0, pol.irreducible_polynomial)
        else:
            return BinPol(self.anti_log[0xFF - self.log[pol.dec].dec].dec, pol.irreducible_polynomial)

    def mul_by_mod(self, a, b):
        if a == 0 or b == 0:
            return 0
        return self.anti_log[(self.log[a].dec + self.log[b].dec) % (2 ** self.grade - 1)].dec
