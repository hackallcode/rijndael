from aes_cipher import AES_128, AES_192, AES_256, AesCipher

KEYS = {
    AES_128: b'qwertyuiasdfghjk',
    AES_192: b'qwertyuiasdfghjkqwertyui',
    AES_256: b'qwertyuiasdfghjkqwertyuiasdfghjk',
}


def test_aes_128_encrypt():
    aes = AesCipher(mode=AES_128)
    encrypted = aes.encrypt(b'1234567890123456', KEYS[AES_128])
    assert encrypted == b'\x04\x43\xfc\x32\x41\xf9\xa4\x41\x3d\xa2\xb0\x2e\xdb\xec\xa3\x5a'


def test_aes_128_decrypt():
    aes = AesCipher(mode=AES_128)
    decrypted = aes.decrypt(b'\x04\x43\xfc\x32\x41\xf9\xa4\x41\x3d\xa2\xb0\x2e\xdb\xec\xa3\x5a', KEYS[AES_128])
    assert decrypted == b'1234567890123456'


def test_aes_192_encrypt():
    aes = AesCipher(mode=AES_192)
    encrypted = aes.encrypt(b'1234567890123456', KEYS[AES_192])
    assert encrypted == b'\xee\x65\xc3\x9b\xa3\x75\x01\x57\x3b\xf6\x4f\x10\xc9\x64\x1a\xaf'


def test_aes_192_decrypt():
    aes = AesCipher(mode=AES_192)
    decrypted = aes.decrypt(b'\xee\x65\xc3\x9b\xa3\x75\x01\x57\x3b\xf6\x4f\x10\xc9\x64\x1a\xaf', KEYS[AES_192])
    assert decrypted == b'1234567890123456'


def test_aes_256_encrypt():
    aes = AesCipher(mode=AES_256)
    encrypted = aes.encrypt(b'1234567890123456', KEYS[AES_256])
    assert encrypted == b'\xf3\x23\xf0\xb1\x1b\x39\xfe\x4b\xb0\xb6\x5f\x10\xb9\x89\xc6\x84'


def test_aes_256_decrypt():
    aes = AesCipher(mode=AES_256)
    decrypted = aes.decrypt(b'\xf3\x23\xf0\xb1\x1b\x39\xfe\x4b\xb0\xb6\x5f\x10\xb9\x89\xc6\x84', KEYS[AES_256])
    assert decrypted == b'1234567890123456'


def test_aes_128_non_multiple_block_size():
    aes = AesCipher(mode=AES_128)
    text = b'Test!'
    encrypted = aes.encrypt(text, KEYS[AES_128])
    assert encrypted == b'\x94\xfd\x7b\xfa\xef\x15\x1e\x84\xe6\x84\xc7\xf0\x8a\xe8\xac\x59'
    decrypted = aes.decrypt(encrypted, KEYS[AES_128])
    assert decrypted == text


def test_aes_192_non_multiple_block_size():
    aes = AesCipher(mode=AES_192)
    text = b'Test!'
    encrypted = aes.encrypt(text, KEYS[AES_192])
    assert encrypted == b'\x12\x32\xbb\xb3\x45\x91\x82\xc5\x04\xd2\x91\xcd\xf2\x30\x20\xa8'
    decrypted = aes.decrypt(encrypted, KEYS[AES_192])
    assert decrypted == text


def test_aes_256_non_multiple_block_size():
    aes = AesCipher(mode=AES_256)
    text = b'Test!'
    encrypted = aes.encrypt(text, KEYS[AES_256])
    assert encrypted == b'\x84\x95\xd9\x28\x81\x97\x96\x30\x15\x3b\x0f\xc5\xc3\xb3\xcf\x60'
    decrypted = aes.decrypt(encrypted, KEYS[AES_256])
    assert decrypted == text
