from bin_pol import BinPol, IrrBinPol
from debugger import Debugger

AES_128 = 1
AES_192 = 2
AES_256 = 3


class AesCipher:
    def __init__(self, mode=AES_128, debug_mode=0):
        self.__d = None
        self.change_mode(mode, debug_mode)

        # Irreducible polynomial used for creating the F256 field
        self.__main_polynomial = IrrBinPol(0b100011011, 3)
        self.__rcon = [
            [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36],
            [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
            [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
            [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        ]

        self.__state = []
        self.__key_schedule = []

    def change_mode(self, mode=None, debug_mode=None):
        if mode is not None:
            self.__nb = 4
            if mode == AES_128:
                self.__nk = 4
                self.__nr = 10
            elif mode == AES_192:
                self.__nk = 6
                self.__nr = 12
            elif mode == AES_256:
                self.__nk = 8
                self.__nr = 14
            elif mode is not None:
                raise Exception('Unknown mode of AES')

        if debug_mode is not None:
            self.__debug_mode = debug_mode

        if mode is not None or debug_mode is not None:
            if self.__d is not None:
                del self.__d
            self.__d = Debugger(self.__debug_mode, f'AES-{32 * self.__nk}')

    @staticmethod
    def __left_shift(array, count=1):
        res = array[:]
        for i in range(count):
            temp = res[1:]
            temp.append(res[0])
            res[:] = temp[:]

        return res

    @staticmethod
    def __left_shift_row(table, row, count=1):
        res = table[:]
        for i in range(count):
            for c in range(len(res) - 1):
                res[c][row], res[c + 1][row] = res[c + 1][row], res[c][row]
        return res

    @staticmethod
    def __right_shift(array, count=1):
        res = array[:]
        for i in range(count):
            tmp = res[:-1]
            tmp.insert(0, res[-1])
            res[:] = tmp[:]

        return res

    @staticmethod
    def __right_shift_row(table, row, count=1):
        res = table[:]
        for i in range(count):
            for c in range(len(table) - 1, 0, -1):
                res[c][row], res[c - 1][row] = res[c - 1][row], res[c][row]
        return res

    def __s_box(self, input_val):
        self.__d.subsubsection('S-box')
        self.__d.debug(f'Input = {self.__d.i(input_val)}')

        s_box_row = input_val // 0x10
        s_box_col = input_val % 0x10
        input_pol = BinPol(16 * s_box_row + s_box_col)

        inv_pol = self.__main_polynomial.inv(input_pol)
        self.__d.inverted(input_pol.dec, inv_pol.dec)

        trans_pol = BinPol(inv_pol.dec)
        for i in range(8):
            trans_pol[i] = inv_pol[i]
            trans_pol[i] ^= inv_pol[(i + 4) % 8]
            trans_pol[i] ^= inv_pol[(i + 5) % 8]
            trans_pol[i] ^= inv_pol[(i + 6) % 8]
            trans_pol[i] ^= inv_pol[(i + 7) % 8]

        trans_pol += BinPol(0b01100011)

        self.__d.affine(trans_pol.bin, [
            [1, 0, 0, 0, 1, 1, 1, 1],
            [1, 1, 0, 0, 0, 1, 1, 1],
            [1, 1, 1, 0, 0, 0, 1, 1],
            [1, 1, 1, 1, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 0, 0, 0],
            [0, 1, 1, 1, 1, 1, 0, 0],
            [0, 0, 1, 1, 1, 1, 1, 0],
            [0, 0, 0, 1, 1, 1, 1, 1],
        ], input_pol.bin, [0, 1, 1, 0, 0, 0, 1, 1], clear_mode=True)

        self.__d.debug(f'Result = {self.__d.i(trans_pol.dec)}')
        return trans_pol.dec

    def __inv_s_box(self, input_val):
        self.__d.subsubsection('S-box')
        self.__d.debug(f'Input = {self.__d.i(input_val)}')

        s_box_row = input_val // 0x10
        s_box_col = input_val % 0x10
        input_pol = BinPol(16 * s_box_row + s_box_col)

        trans_pol = BinPol(input_val)
        for i in range(8):
            trans_pol[i] = input_pol[(i + 2) % 8]
            trans_pol[i] ^= input_pol[(i + 5) % 8]
            trans_pol[i] ^= input_pol[(i + 7) % 8]

        trans_pol += BinPol(0b00000101)

        self.__d.affine(trans_pol.bin, [
            [0, 0, 1, 0, 0, 1, 0, 1],
            [1, 0, 0, 1, 0, 0, 1, 0],
            [0, 1, 0, 0, 1, 0, 0, 1],
            [1, 0, 1, 0, 0, 1, 0, 0],
            [0, 1, 0, 1, 0, 0, 1, 0],
            [0, 0, 1, 0, 1, 0, 0, 1],
            [1, 0, 0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 1, 0, 1, 0],
        ], input_pol.bin, [1, 0, 1, 0, 0, 0, 0, 0], clear_mode=True)

        inv_pol = self.__main_polynomial.inv(trans_pol)
        self.__d.inverted(trans_pol.dec, inv_pol.dec)

        self.__d.debug(f'Result = {self.__d.i(trans_pol.dec)}')
        return inv_pol.dec

    def __key_expansion(self, key):
        self.__d.pause()

        key_symbols = [symbol for symbol in key]
        if len(key_symbols) < 4 * self.__nk:
            for i in range(4 * self.__nk - len(key_symbols)):
                key_symbols.append(0x01)

        self.__key_schedule = []
        for col in range(self.__nk):
            self.__key_schedule.append([])
            for row in range(4):
                self.__key_schedule[col].append(key_symbols[row + 4 * col])

        for col in range(self.__nk, self.__nb * (self.__nr + 1)):
            self.__key_schedule.append([])
            if col % self.__nk == 0:
                xor_col = self.__left_shift(self.__key_schedule[col - 1])
                for row in range(4):
                    xor_col[row] = self.__s_box(xor_col[row])
                    xor_col[row] ^= self.__rcon[row][int(col / self.__nk - 1)]
            elif self.__nk > 6 and col % self.__nk == 4:
                xor_col = self.__key_schedule[col - 1][:]
                for row in range(4):
                    xor_col[row] = self.__s_box(xor_col[row])
            else:
                xor_col = self.__key_schedule[col - 1]

            for row in range(4):
                s = self.__key_schedule[col - self.__nk][row] ^ xor_col[row]
                self.__key_schedule[col].append(s)

        self.__d.play()
        return self.__key_schedule

    def __sub_bytes(self, decryption=False):
        self.__d.subsection('Sub Bytes')
        self.__d.debug('State before step:')
        self.__d.table(self.__state)

        if not decryption:
            s_box = self.__s_box
        else:
            s_box = self.__inv_s_box

        for i in range(len(self.__state)):
            for j in range(len(self.__state[i])):
                self.__state[i][j] = s_box(self.__state[i][j])

        self.__d.debug('State after step:')
        self.__d.table(self.__state)

    def __shift_rows(self, decryption=False):
        self.__d.subsection('Shift Rows')
        self.__d.debug('State before step:')
        self.__d.table(self.__state)

        if not decryption:
            shift = self.__left_shift_row
        else:
            shift = self.__right_shift_row

        for row in range(1, 4):
            self.__state = shift(self.__state, row, row)

        self.__d.debug('State after step:')
        self.__d.table(self.__state)

    def __mul_by(self, input_val, mod):
        return self.__main_polynomial.mul_by_mod(input_val, mod)

    def __mix_columns(self, decryption=False):
        self.__d.subsection('Mix Columns')
        self.__d.debug('State before step:')
        self.__d.table(self.__state)

        for col in range(self.__nb):
            if not decryption:
                s0 = self.__mul_by(self.__state[col][0], 2) ^ self.__mul_by(self.__state[col][1], 3) ^ \
                     self.__state[col][2] ^ self.__state[col][3]

                s1 = self.__state[col][0] ^ self.__mul_by(self.__state[col][1], 2) ^ \
                     self.__mul_by(self.__state[col][2], 3) ^ self.__state[col][3]

                s2 = self.__state[col][0] ^ self.__state[col][1] ^ \
                     self.__mul_by(self.__state[col][2], 2) ^ self.__mul_by(self.__state[col][3], 3)

                s3 = self.__mul_by(self.__state[col][0], 3) ^ self.__state[col][1] ^ \
                     self.__state[col][2] ^ self.__mul_by(self.__state[col][3], 2)

            else:
                s0 = self.__mul_by(self.__state[col][0], 14) ^ self.__mul_by(self.__state[col][1], 11) ^ \
                     self.__mul_by(self.__state[col][2], 13) ^ self.__mul_by(self.__state[col][3], 9)

                s1 = self.__mul_by(self.__state[col][0], 9) ^ self.__mul_by(self.__state[col][1], 14) ^ \
                     self.__mul_by(self.__state[col][2], 11) ^ self.__mul_by(self.__state[col][3], 13)

                s2 = self.__mul_by(self.__state[col][0], 13) ^ self.__mul_by(self.__state[col][1], 9) ^ \
                     self.__mul_by(self.__state[col][2], 14) ^ self.__mul_by(self.__state[col][3], 11)

                s3 = self.__mul_by(self.__state[col][0], 11) ^ self.__mul_by(self.__state[col][1], 13) ^ \
                     self.__mul_by(self.__state[col][2], 9) ^ self.__mul_by(self.__state[col][3], 14)

            self.__d.affine([s0, s1, s2, s3], [
                [2, 3, 1, 1],
                [1, 2, 3, 1],
                [1, 1, 2, 3],
                [3, 1, 1, 2],
            ], self.__state[col])

            self.__state[col][0] = s0
            self.__state[col][1] = s1
            self.__state[col][2] = s2
            self.__state[col][3] = s3

        self.__d.debug('State after step:')
        self.__d.table(self.__state)

    def __add_round_key(self, round_num=0):
        self.__d.subsection(f'Add Round Key (round = {round_num})')
        self.__d.debug('State before step:')
        self.__d.table(self.__state)
        self.__d.debug('Key schedule matrix:')
        self.__d.table(self.__key_schedule, self.__nb * round_num)

        for col in range(self.__nb):
            self.__state[col][0] ^= self.__key_schedule[self.__nb * round_num + col][0]
            self.__state[col][1] ^= self.__key_schedule[self.__nb * round_num + col][1]
            self.__state[col][2] ^= self.__key_schedule[self.__nb * round_num + col][2]
            self.__state[col][3] ^= self.__key_schedule[self.__nb * round_num + col][3]

        self.__d.debug('State after step:')
        self.__d.table(self.__state)

    def __data_to_state(self, data):
        self.__state = []
        for c in range(self.__nb):
            self.__state.append([])
            for r in range(4):
                self.__state[c].append(data[r + 4 * c])

    def __state_to_data(self):
        output = []
        for c in range(self.__nb):
            for r in range(4):
                output.append(self.__state[c][r])
        return output

    def __encrypt_block(self, data):
        self.__data_to_state(data)
        self.__d.subsection('Initial conditions')
        self.__d.debug('State before encryption:')
        self.__d.table(self.__state)

        for rnd in range(0, self.__nr):
            self.__add_round_key(rnd)
            self.__sub_bytes()
            self.__shift_rows()
            if rnd != self.__nr - 1:
                self.__mix_columns()
        self.__add_round_key(self.__nr)

        self.__d.subsection('Result')
        self.__d.debug('State after encryption:')
        self.__d.table(self.__state)
        return self.__state_to_data()

    def encrypt(self, data, key, mode=None, debug_mode=None):
        self.__d.chapter('Encryption')
        self.change_mode(mode, debug_mode)
        self.__key_expansion(key)

        encrypted_data = []
        while len(data) % 16 != 0:
            data += b'\x00'
        for i in range(int(len(data) / 16)):
            self.__d.section(f'Encryption of block \\#{i}')
            encrypted_part = self.__encrypt_block(data[i * 16:(i + 1) * 16])
            encrypted_data.extend(encrypted_part)
        return bytes(encrypted_data)

    def __decrypt_block(self, data):
        self.__data_to_state(data)
        self.__d.subsection('Initial conditions')
        self.__d.debug('State before decryption:')
        self.__d.table(self.__state)

        self.__add_round_key(self.__nr)
        for rnd in range(self.__nr - 1, -1, -1):
            if rnd != self.__nr - 1:
                self.__mix_columns(decryption=True)
            self.__shift_rows(decryption=True)
            self.__sub_bytes(decryption=True)
            self.__add_round_key(rnd)

        self.__d.subsection('Result')
        self.__d.debug('State after decryption:')
        self.__d.table(self.__state)
        return self.__state_to_data()

    def decrypt(self, data, key, mode=None, debug_mode=None):
        assert len(data) % 16 == 0, 'Data must be aligned to 16 bytes'

        self.__d.chapter('Decryption')
        self.change_mode(mode, debug_mode)
        self.__key_expansion(key)

        decrypted_data = []
        for i in range(int(len(data) / 16)):
            self.__d.section(f'Decryption of block \\#{i}')
            decrypted_part = self.__decrypt_block(data[i * 16:(i + 1) * 16])
            decrypted_data.extend(decrypted_part)
        while decrypted_data[-1] == 0:
            decrypted_data = decrypted_data[:-1]
        return bytes(decrypted_data)
